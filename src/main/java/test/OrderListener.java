package test;

import com.espertech.esper.client.EPAdministrator;
import com.espertech.esper.client.EPRuntime;
import com.espertech.esper.client.EPServiceProvider;
import com.espertech.esper.client.EPServiceProviderManager;
import com.espertech.esper.client.EPStatement;
import com.espertech.esper.client.EventBean;
import com.espertech.esper.client.UpdateListener;

public class OrderListener implements UpdateListener {

    public void update(EventBean[] newEvents, EventBean[] oldEvents) {
        EventBean eb = newEvents[0];
        System.out.println(eb.get("avgPrice"));
    }

}